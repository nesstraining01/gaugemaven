package gauge_maven;

import com.thoughtworks.gauge.*;
import gauge_maven.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class StepImplementation {

    private HashSet<Character> vowels;
    public static WebDriver driver;
    private FacebookLoginPage facebookLoginPage;
    private FacebookWelcomePage facebookWelcomePage;
    private FacebookIncorrectLoginPage facebookIncorrectLoginPage;
    private FacebookMakePostPage facebookMakePostPage;
    private FacebookListOfPostsPage facebookListOfPostsPage;
    private FacebookDeletePostPage facebookDeletePostPage;

    @BeforeScenario
    public void openBrowserWithLink() throws Throwable {

        //switch (browser) {
//            case "Firefox" :
//                if (operatingSystem.equalsIgnoreCase("Windows")) {
//                    System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/"+ operatingSystem +"/geckodriver.exe");
//                } else {
//                    System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/" + operatingSystem + "/geckodriver");
//                }
//                driver = new FirefoxDriver();
//                break;
        // case "Chrome" :
//                if (operatingSystem.equalsIgnoreCase("Windows")) {

 //       System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/" + "WINDOWS" + "/chromedriver.exe");
//                } else {
//        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/" + "MacOS" + "/chromedriver");


        //                }
//                // DISABLE CHROME NOTIFICATIONS:
//                // Create object of HashMap Class
//                Map<String, Object> prefs = new HashMap<String, Object>();
//                // Set the notification setting it will override the default setting
//                prefs.put("profile.default_content_setting_values.notifications", 2);
//                // Create object of ChromeOption class
//                ChromeOptions options = new ChromeOptions();
//                // Set the experimental option
//                options.setExperimentalOption("prefs", prefs);
//                driver = new ChromeDriver(options);
//                break;
//        }
        driver = DriverFactory.getDriver();
        driver.get(System.getenv("LINK"));
    }


    @Step("Vowels in English language are <vowelString>.")
    public void setLanguageVowels(String vowelString) {
        vowels = new HashSet<>();
        for (char ch : vowelString.toCharArray()) {
            vowels.add(ch);
        }
    }

    @Step("The word <word> has <expectedCount> vowels.")
    public void verifyVowelsCountInWord(String word, int expectedCount) {
        int actualCount = countVowels(word);
        assertThat(expectedCount).isEqualTo(actualCount);
    }

    @Step("Almost all words have vowels <wordsTable>")
    public void verifyVowelsCountInMultipleWords(Table wordsTable) {
        for (TableRow row : wordsTable.getTableRows()) {
            String word = row.getCell("Word");
            int expectedCount = Integer.parseInt(row.getCell("Vowel Count"));
            int actualCount = countVowels(word);

            assertThat(expectedCount).isEqualTo(actualCount);
        }
    }

    private int countVowels(String word) {
        int count = 0;
        for (char ch : word.toCharArray()) {
            if (vowels.contains(ch)) {
                count++;
            }
        }
        return count;
    }

    @Step ("I successfully login to facebook with email <email> and password <password>")
    public void successfulLoginToFacebookWithCredentials(String email, String password) throws Throwable {
        facebookLoginPage = new FacebookLoginPage(driver);
        facebookIncorrectLoginPage = facebookLoginPage.unSuccessfulLogin(email, password);
    }

    @AfterScenario
    public void closeBrowser() {
        driver.quit();
    }

}
