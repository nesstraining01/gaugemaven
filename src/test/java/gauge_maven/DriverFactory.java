package gauge_maven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverFactory {

    // Get a new WebDriver Instance.
    // There are various implementations for this depending on browser. The required browser can be set as an environment variable.
    // Refer http://getgauge.io/documentation/user/current/managing_environments/README.html
    public static WebDriver getDriver() {

        String browser = System.getenv("BROWSER");
        switch (browser)
        {
            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/" + "WINDOWS" + "/geckodriver.exe");
                return new FirefoxDriver();
            case "IE":
                System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/" + "WINDOWS" + "/geckodriver.exe");
                return new InternetExplorerDriver();
            default:
                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/" + "WINDOWS" + "/chromedriver.exe");
                return new ChromeDriver();

        }
    }
}
